package errors

import "github.com/gin-gonic/gin"

// BuildErrorResponse building err response for gin
func BuildErrorResponse(ctx *gin.Context, status int, err error) {
	httpError := HTTPError{Message: err.Error()}
	ctx.JSON(status, httpError)
}

// HTTPError http err response schema
type HTTPError struct {
	Message string `json:"message" example:"status bad request"`
}
