module gitlab.com/id-access-manager/iam-pkgs/gorm

go 1.21.6

require (
	github.com/go-playground/assert/v2 v2.2.0
	gorm.io/gorm v1.25.7
)

require (
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
)
