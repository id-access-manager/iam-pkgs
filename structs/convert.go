package structs

import (
	"errors"
	"reflect"
)

func ConvertToMap(_struct any) (map[string]any, error) {

	returnMap := make(map[string]interface{})

	sType := getStructType(_struct)

	if sType.Kind() != reflect.Struct {
		return returnMap, errors.New("variable given is not a struct or a pointer to a struct")
	}

	for i := 0; i < sType.NumField(); i++ {
		structFieldName := sType.Field(i).Name
		structVal := reflect.ValueOf(_struct)
		returnMap[structFieldName] = structVal.FieldByName(structFieldName).Interface()
	}

	return returnMap, nil
}

func getStructType(_struct any) reflect.Type {
	sType := reflect.TypeOf(_struct)
	if sType.Kind() == reflect.Ptr {
		sType = sType.Elem()
	}

	return sType
}
